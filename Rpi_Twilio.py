# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client
import RPi.GPIO as GPIO


# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
# You will Find SID and TOKEN on Twilio's Dashboard page. Just copy and paste them.
account_sid = 'Paste Your SID Here'
auth_token = 'Paste Your Token Here'
client = Client(account_sid, auth_token)
count=0
msg=0

PIR_input = 29				#read PIR Output
LED = 32				#LED for signalling motion detected	
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)		#choose pin no. system
GPIO.setup(PIR_input, GPIO.IN)	


def send(msg):
    message = client.messages \
        .create(
             from_='whatsapp:+14xxxxxxxx',
             body=msg,
             to='whatsapp:+9185xxxxxx'
         )

    print(message.sid)

while 1:
    if(GPIO.input(PIR_input)):
        msg="*Alert!!! Somebody At Home*"
        
        if(count==0):
            send(msg)
            count=1
            
    else:
        if(count==1):
            count=0
        
